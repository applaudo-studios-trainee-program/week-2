/*
  These are the different requests that are made during execution
*/

const PROXY = 'https://astpw2.herokuapp.com/';

// Gets the options according what the user has typed

export const getOptionsByUserTyping = async (userTyping) => {
  const URL = `${PROXY}https://www.metaweather.com/api/location/search/?query=${encodeURI(
    userTyping
  )}`;

  const response = await fetch(URL);
  const body = await response.json();

  return body.map((option) => option.title);
};

// Gets the weather according a WOEID (Where On Earth ID)

export const getWeather = async (WOEID) => {
  localStorage.setItem('lastSearchWOEID', WOEID);

  const URL = `${PROXY}https://www.metaweather.com/api/location/${WOEID}/`;

  const response = await fetch(URL);
  const body = await response.json();

  const {
    consolidated_weather: weatherData,
    time,
    timezone,
    parent: { title: stateName },
    title: cityName,
  } = body;

  const location = `${cityName}, ${stateName}`;

  return { weatherData, time, timezone, location };
};

// Gets WOEID (Where On Earth ID) according a given location

export const getWOEID = async (woeid) => {
  const URL = `${PROXY}https://www.metaweather.com/api/location/search/?query=${encodeURI(
    woeid
  )}`;

  const response = await fetch(URL);
  const body = await response.json();

  return body[0].woeid;
};
