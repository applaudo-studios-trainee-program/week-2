import {
  getOptionsByUserTyping,
  getWeather,
  getWOEID,
} from './services/fetchServices.js';

import { getTimeByTimeZone } from './helpers/getTimeByTimeZone.js';
import { setWeather } from './helpers/setWeather.js';
import { setOptionsByUserTyping } from './helpers/setOptionsByUserTyping.js';
import { setTodaysWeather } from './helpers/setTodaysWeather.js';
import { setBackgroundByTime } from './helpers/setBackgroundByTime.js';
import { loaderHandler } from './helpers/loaderHandler.js';
import { debounceHandler } from './helpers/debounceHandler.js';
import { removeElementList } from './helpers/removeElementList.js';
import { setErrorOnFailedRequest } from './helpers/setErrorOnFailedRequest.js';

{
  const form = document.querySelector('.city-search');
  const input = document.querySelector('.city-search__input');

  window.addEventListener('DOMContentLoaded', async () => {
    loaderHandler();

    const woeid = localStorage.getItem('lastSearchWOEID') || '2487956';

    const { weatherData, location, time, timezone } = await getWeather(woeid);

    const actualTime = getTimeByTimeZone(time, timezone);

    setBackgroundByTime(actualTime);

    weatherData.map((weather, index) => {
      if (index === 0) setTodaysWeather({ ...weather, location });

      setWeather({ ...weather });
    });

    loaderHandler();
  });

  input.addEventListener(
    'input',
    debounceHandler(async () => {
      const inputValue = input.value.trim();

      if (inputValue !== '') {
        const options = await getOptionsByUserTyping(inputValue);

        const lasOptions = [
          ...document.querySelectorAll('#city-list > option'),
        ];

        removeElementList(lasOptions);
        setOptionsByUserTyping(options);
      }
    }, 1000)
  );

  form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const inputValue = input.value.trim();

    if (inputValue !== '') {
      const todaysWeatherList = [
        ...document.querySelectorAll('.todays-weather > *'),
      ];

      const weatherList = [
        ...document.querySelectorAll('.weather-list__container > *'),
      ];

      try {
        input.value = '';
        input.blur();

        loaderHandler();

        const woeid = await getWOEID(inputValue);

        const { weatherData, location, time, timezone } = await getWeather(
          woeid
        );

        const actualTime = getTimeByTimeZone(time, timezone);

        setBackgroundByTime(actualTime);

        removeElementList(todaysWeatherList);
        removeElementList(weatherList);

        weatherData.map((weather, index) => {
          if (index === 0) setTodaysWeather({ ...weather, location });

          setWeather({ ...weather });
        });

        loaderHandler();
      } catch {
        removeElementList(todaysWeatherList);
        removeElementList(weatherList);

        setErrorOnFailedRequest();

        loaderHandler();
      }
    }
  });
}
