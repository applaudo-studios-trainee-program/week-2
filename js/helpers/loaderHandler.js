/*
  Handles the logic to set a loader
*/

export const loaderHandler = () => {
  const loader = document.querySelector('.loader-container');
  const isLoaderInDOM = document.body.contains(loader);

  if (isLoaderInDOM) loader.remove();
  else {
    const loaderContainer = document.createElement('div');
    const loader = document.createElement('span');
    const loaderInner = document.createElement('span');

    loaderInner.classList.add('loader-inner');

    loader.classList.add('loader');
    loader.append(loaderInner);

    loaderContainer.classList.add('loader-container');
    loaderContainer.append(loader);

    document.body.append(loaderContainer);
  }
};
