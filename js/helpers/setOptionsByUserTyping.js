/*
  Sets the list of options according the type of the user
*/

export const setOptionsByUserTyping = (options) => {
  options.forEach((option) => {
    const cityList = document.querySelector('#city-list');

    const optionTag = document.createElement('option');

    optionTag.append(option);

    cityList.append(optionTag);
  });
};
