/*
  Handles the logic to remove a list of given elements
*/

export const removeElementList = (elementList) => {
  elementList.forEach((element) => element.remove());
};
