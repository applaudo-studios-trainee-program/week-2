/*
  Sets an error when a request fails
*/

export const setErrorOnFailedRequest = () => {
  const todaysWeather = document.querySelector('.todays-weather');
  const weatherList = document.querySelector('.weather-list__container');

  const errorSadFace = document.createElement('h1');
  const errorText = document.createElement('h2');

  errorSadFace.classList.add('error__sad-face');
  errorSadFace.append('😔');

  errorText.classList.add('error__text');
  errorText.append("We couldn't find anything, please type another city");

  todaysWeather.append(errorSadFace);

  weatherList.append(errorText);
};
