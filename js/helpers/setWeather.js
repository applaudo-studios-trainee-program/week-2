/*
  Sets the list of the next weathers
*/

const DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

export const setWeather = ({
  applicable_date: date,
  min_temp: minTemp,
  max_temp: maxTemp,
  weather_state_abbr: iconAbbreviation,
  weather_state_name: weatherState,
}) => {
  const containerReference = document.querySelector('.weather-list__container');

  const weather = document.createElement('div');
  const weatherContent = document.createElement('div');
  const weatherTemps = document.createElement('p');
  const weatherMinTemp = document.createElement('span');
  const weatherMaxTemp = document.createElement('span');
  const weatherIcon = document.createElement('img');
  const weatherDay = document.createElement('p');

  weatherMinTemp.classList.add('weather__min-temp');
  weatherMinTemp.innerHTML = `${Math.round(minTemp)} &deg;`;

  weatherMaxTemp.classList.add('weather__max-temp');
  weatherMaxTemp.innerHTML = `${Math.round(maxTemp)} &deg;`;

  weatherTemps.classList.add('weather__temps');
  weatherTemps.append(weatherMaxTemp);
  weatherTemps.append(' | ');
  weatherTemps.append(weatherMinTemp);

  weatherIcon.classList.add('weather__icon');
  weatherIcon.setAttribute(
    'src',
    `https://www.metaweather.com/static/img/weather/${iconAbbreviation}.svg`
  );

  weatherIcon.setAttribute('alt', 'Weather Icon');
  weatherIcon.setAttribute('title', weatherState);

  weatherDay.classList.add('weather__day');
  weatherDay.append(DAYS[new Date(date).getDay()]);

  weatherContent.classList.add('weather__content');
  weatherContent.append(weatherDay);
  weatherContent.append(weatherTemps);

  weather.classList.add('weather');
  weather.append(weatherIcon);
  weather.append(weatherContent);

  containerReference.append(weather);
};
