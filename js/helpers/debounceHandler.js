/*
  Handles the logic to make a debounce
*/

export const debounceHandler = (callback, delay) => {
  let timeoutID;

  return () => {
    timeoutID && clearTimeout(timeoutID);
    timeoutID = setTimeout(callback, delay);
  };
};
