/*
  Sets a background according a time zone
*/

const BACKGROUNDS = {
  morning: 'statics/images/morning.jpg',
  afternoon: 'statics/images/afternoon.jpg',
  night: 'statics/images/nigth.png',
};

export const setBackgroundByTime = (time) => {
  const app = document.querySelector('.app');
  const numericTime = parseInt(time);

  if (numericTime >= 4 && numericTime <= 10) {
    app.style.backgroundImage = `url(${BACKGROUNDS.morning})`;
  }

  if (numericTime >= 11 && numericTime <= 17) {
    app.style.backgroundImage = `url(${BACKGROUNDS.afternoon})`;
  }

  if (
    (numericTime >= 18 && numericTime <= 24) ||
    (numericTime >= 0 && numericTime <= 3)
  ) {
    app.style.backgroundImage = `url(${BACKGROUNDS.night})`;
  }
};
