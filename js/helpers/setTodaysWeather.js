/*
  Sets the weather of the actual day when the request is made
*/

export const setTodaysWeather = ({
  weather_state_name: weatherState,
  the_temp: temperature,
  location,
}) => {
  const containerReference = document.querySelector('.todays-weather');

  const todayWeatherState = document.createElement('p');
  const todaysWeatherCityName = document.createElement('h1');
  const todaysWeatherTemperature = document.createElement('h2');

  todayWeatherState.classList.add('todays-weather__weather-state');
  todayWeatherState.append(weatherState);

  todaysWeatherCityName.classList.add('todays-weather__city-name');
  todaysWeatherCityName.append(location);

  todaysWeatherTemperature.classList.add('todays-weather__temperature');
  todaysWeatherTemperature.innerHTML = `${Math.round(temperature)} &deg;`;

  containerReference.append(todayWeatherState);
  containerReference.append(todaysWeatherCityName);
  containerReference.append(todaysWeatherTemperature);
};
