/*
  Returns a time according a time zone
*/

export const getTimeByTimeZone = (time, timeZone) => {
  const options = { timeZone, hour: 'numeric', hour12: false };

  const dateTime = new Intl.DateTimeFormat([], options);

  return dateTime.format(new Date(time));
};
