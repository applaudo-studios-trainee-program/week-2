# Trainee Program - Week 2

## Installation

There is **no necessary configuration** but you have to run the project on a server or the modules are not going to work

## Why the modules are not going to work?

Because the `file://` protocol doesn't support the import/export directives, so **I highly recommend** to use one of the link(s) below to run the project

## Running the Project

You can use the link(s) below to see the last deployed version of the project

[GitLab Pages](https://applaudo-studios-trainee-program.gitlab.io/week-2/)  
[Vercel](https://week-2.b-mendoza.vercel.app/)
